from django.contrib import admin

from .models import Track
from actions import export_as_excel

class TrackAdmin(admin.ModelAdmin):
    list_display = ('title', 'artist', 'album', 'order', 'player', 'is_firsttrack')
    list_filter = ('artist', 'album')
    search_fields = ('title', 'artist__first_name','artist__last_name')
    list_editable = ('order', 'album', 'artist')
    actions = (export_as_excel,)
    raw_id_fields = ('artist',)
    def is_firsttrack(self, obj):
        return obj.order == 1
    is_firsttrack.boolean = True

admin.site.register(Track, TrackAdmin)
